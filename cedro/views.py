from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.http import HttpResponse

from tools import escreve, consulta, cria_mnemonico

from models import Formulario, Campo

from unidecode import unidecode
import json

# Create your views here.
def home(request):

	contexto = {}

	return render_to_response('home.htm', contexto, context_instance=RequestContext(request))

def consulta(request, nr):

	cursor = consulta(nr)
	contexto = { 'lista' : cursor }

	return render_to_response('consulta.htm', contexto, context_instance=RequestContext(request))

def consulta_campo(request, nr, campo):

	result = consulta(nr)
	
	return HttpResponse(str(result[campo]))

def consulta_raw(request, nr):

	result = consulta(nr)
	return HttpResponse(str(result))

def formulario(request, nr):

	if request.method == 'POST':
		
		formulario = Formulario.objects.get(nr=nr)

		documento = {}
		for campo in request.POST:
			if campo == 'csrfmiddlewaretoken'
				continue

			documento[campo] = request.POST[campo]

		documento['aprovado'] = False;


		#escreve()

		saida = json.dumps(request.POST, sort_keys=True, indent=4, separators=(',', ': '))
		print saida
		for chave in request.POST:
			print chave, unidecode(request.POST[chave])
		return HttpResponse(saida)

	try:
		formulario = Formulario.objects.get(nr=nr)
	except Exception, e:
		raise e

	contexto = {
		'nr': formulario.nr,
		'nome': formulario.nome,
		'descricao': formulario.descricao,
		'campos': []
	}

	for campo in formulario.campos.order_by('ordem'):
		opcoes = [opcao.strip() for opcao in campo.meta.split('\n')]
		mne_opcoes = map(cria_mnemonico, opcoes)
		lista_opcoes = [{'nome': opcoes[i], 'mne': mne_opcoes[i]} for i in range(len(opcoes))]
		campo = {
			'nome': campo.nome,
			'mnemonico': cria_mnemonico(campo.nome),
			'descricao': campo.descricao,
			'tipo': campo.tipo,
			'opcoes': lista_opcoes,
			'requerido': campo.requerido,
		}

		contexto['campos'].append(campo)

	return render_to_response('tpl/form.htm', contexto, context_instance=RequestContext(request))