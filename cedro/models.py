from django.db import models
from django.contrib.auth.models import User

class Formulario(models.Model):
    nr = models.CharField(max_length=200, unique=True)
    nome = models.CharField(max_length=255)
    descricao = models.TextField()
    html = models.TextField(null=True, default=None)
    doc_publico = models.BooleanField(default=False)
    criacao = models.DateTimeField(auto_now_add=True)

class Campo(models.Model):
    formulario = models.ForeignKey(Formulario, related_name='campos')
    nome = models.CharField(max_length=255)
    mnemonico = models.CharField(max_length=255)
    descricao = models.TextField()
    requerido = models.BooleanField(default=True)
    tipo = models.CharField(max_length=255)
    meta = models.TextField()
    ordem = models.IntegerField()

class AcessoFormulario(models.Model):
    usuario = models.ForeignKey(User, related_name='formularios')
    formulario = models.ForeignKey(Formulario, related_name='acessos')
    pode_ler = models.BooleanField()
    pode_escrever = models.BooleanField()
    criacao = models.DateTimeField(auto_now_add=True)