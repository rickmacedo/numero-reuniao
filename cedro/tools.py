# -*- encoding: utf-8 -*-
from nr.settings import mongo
import datetime
import re
import random
from unidecode import unidecode

def escreve(nr, tipo, dados, meta='', publico=True):
	mongo.cedro.docs.insert_one({
		'_id': nr,
		'_tipo': tipo,
		'_timestamp': datetime.datetime.utcnow(),
		'_dados': dados,
		'_publico': publico,
		'_meta': meta
		})
	

def consulta(nr):
	dado = mongo.cedro.docs.find_one({ '_id' : str(nr) })
	if dado is not None:
		return dado['_dados']

	return None

def pesquisa(campo, valor):
	pass

def cria_mnemonico(entrada):
	# remove acentos e maiúsculas
	saida = unidecode(entrada).lower()

	# remove tudo que não for a-zA-Z0-9_ e espaços nas pontas
	saida = re.sub('(\W)+', ' ', saida).strip()

	# substitui espaços por hífens
	saida = re.sub('( )+', '-', saida)

	return saida

def gera_nr(prefixo='', sufixo=''):

	testa_nr = True
	while testa_nr:
		nr = prefixo + str(random.randint(1,999999999)).zfill(9) + sufixo

		result = mongo.cedro.docs.find_one({'_id'})
		
		if result is None:
			testa_nr = False

	return nr