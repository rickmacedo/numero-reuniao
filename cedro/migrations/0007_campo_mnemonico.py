# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cedro', '0006_auto_20151103_2333'),
    ]

    operations = [
        migrations.AddField(
            model_name='campo',
            name='mnemonico',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
    ]
