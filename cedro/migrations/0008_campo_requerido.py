# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cedro', '0007_campo_mnemonico'),
    ]

    operations = [
        migrations.AddField(
            model_name='campo',
            name='requerido',
            field=models.BooleanField(default=True),
        ),
    ]
