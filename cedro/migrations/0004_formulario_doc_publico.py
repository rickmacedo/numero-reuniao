# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cedro', '0003_formulario_html'),
    ]

    operations = [
        migrations.AddField(
            model_name='formulario',
            name='doc_publico',
            field=models.BooleanField(default=False),
        ),
    ]
