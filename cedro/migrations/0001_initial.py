# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AcessoFormulario',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pode_ler', models.BooleanField()),
                ('pode_escrever', models.BooleanField()),
                ('criacao', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Campo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=255)),
                ('descricao', models.TextField()),
                ('tipo', models.CharField(max_length=255)),
                ('ordem', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Formulario',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nr', models.CharField(max_length=200)),
                ('nome', models.CharField(max_length=255)),
                ('descricao', models.TextField()),
            ],
        ),
        migrations.AddField(
            model_name='campo',
            name='formulario',
            field=models.ForeignKey(related_name='campos', to='cedro.Formulario'),
        ),
        migrations.AddField(
            model_name='acessoformulario',
            name='formulario',
            field=models.ForeignKey(related_name='acessos', to='cedro.Formulario'),
        ),
        migrations.AddField(
            model_name='acessoformulario',
            name='usuario',
            field=models.ForeignKey(related_name='formularios', to=settings.AUTH_USER_MODEL),
        ),
    ]
