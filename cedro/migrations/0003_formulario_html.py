# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cedro', '0002_formulario_criacao'),
    ]

    operations = [
        migrations.AddField(
            model_name='formulario',
            name='html',
            field=models.TextField(null=True),
        ),
    ]
