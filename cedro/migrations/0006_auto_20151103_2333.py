# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cedro', '0005_auto_20151103_2253'),
    ]

    operations = [
        migrations.AlterField(
            model_name='formulario',
            name='nr',
            field=models.CharField(unique=True, max_length=200),
        ),
    ]
