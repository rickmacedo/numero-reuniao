# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cedro', '0004_formulario_doc_publico'),
    ]

    operations = [
        migrations.AddField(
            model_name='campo',
            name='meta',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='formulario',
            name='html',
            field=models.TextField(default=None, null=True),
        ),
    ]
