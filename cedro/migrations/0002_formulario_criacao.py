# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('cedro', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='formulario',
            name='criacao',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 2, 15, 12, 39, 959446, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
    ]
