# -*- encoding: utf-8 -*-
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout

from django.contrib import messages

import cedro
from cedro.models import Formulario, Campo, AcessoFormulario

import random


def home(request):

	contexto = {}

	if request.method == "POST":

		username = request.POST['usuario']
		senha = request.POST['senha']

		try:
			usuario = authenticate(username=username, password=senha)
			
			if usuario is None:
				raise Exception('Usuário e/ou senha inválidos.')

			login(request, usuario)

			return render_to_response('panel.htm', contexto, context_instance=RequestContext(request))

		except Exception, e:
			contexto['erroauth'] = True
			return render_to_response('main.htm', contexto, context_instance=RequestContext(request))
		
	

	if request.user.is_authenticated():
		return render_to_response('panel.htm', contexto, context_instance=RequestContext(request))

	return render_to_response('main.htm', contexto, context_instance=RequestContext(request))


def logout_view(request):

	logout(request)

	return redirect('/')


def inbox(request):

	return render_to_response('internas/caixaentrada.htm')

@login_required
def formularios(request):

	acessos = AcessoFormulario.objects.filter(usuario=request.user)

	contexto = { 'formularios': [] }
	for acesso in acessos:
		form = {
			'nr': acesso.formulario.nr,
			'nome': acesso.formulario.nome,
			'descricao': acesso.formulario.descricao,
			'criacao': acesso.formulario.criacao,
			'pode_ler': acesso.pode_ler,
			'pode_escrever': acesso.pode_escrever
		}
		contexto['formularios'].append(form)

	return render_to_response('internas/formularios.htm', contexto, context_instance=RequestContext(request))

@login_required
def formularios_novo(request):
	contexto = {}

	contexto['dados'] = { 'nr': 'FORM' + str(random.randint(1,999999999)).zfill(9) }
	
	if request.method == "POST":
		dados = {
			'nr': request.POST['nr'],
			'nome': request.POST['nome'],
			'descricao': request.POST['descricao'],
			'docpublico': request.POST['docpublico'],
			'campos': []
		}

		i = 0
		while (('nomecampo' + str(i)) in request.POST):
			s_i = str(i)

			if len(request.POST['nomecampo' + s_i]) == 0:
				i = i + 1
				continue

			dados['campos'].append({
					'nome': request.POST['nomecampo' + s_i],
					'tipo': request.POST['tipo' + s_i],
					'op': request.POST['opcampo' + s_i] or '',
					'desc': request.POST['desccampo' + s_i] or '',
					'ordem': int(request.POST['ordem' + s_i]) or 0
				})

			i = i + 1

		# grava no cedro
		cedro.escreve(dados['nr'], 'form', dados)

		# cria formulario
		formulario = Formulario()
		formulario.nr = dados['nr']
		formulario.nome = dados['nome']
		formulario.descricao = dados['descricao']
		formulario.doc_publico = dados['docpublico']
		formulario.save()

		# cria campos
		for in_campo in dados['campos']:
			campo = Campo()
			campo.formulario = formulario
			campo.nome = in_campo['nome']
			campo.descricao = in_campo['desc']
			campo.tipo = in_campo['tipo']
			campo.meta = in_campo['op']
			campo.ordem = in_campo['ordem']
			campo.save()

		# cria acesso
		acesso = AcessoFormulario()
		acesso.usuario = request.user
		acesso.formulario = formulario
		acesso.pode_ler = True
		acesso.pode_escrever = True
		acesso.save()

		messages.success(request, u'Formulário <strong>' + dados['nr'] + '</strong> adicionado com sucesso.')
		return redirect('/i/forms/')

		contexto['dados'] = dados
		print dados

	return render_to_response('internas/novoform.htm', contexto, context_instance=RequestContext(request))


def exibe_formulario(request, nr):
	pass

def novo_formulario(request):
	pass

