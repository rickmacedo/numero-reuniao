from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Perfil(models.Model):
    user = models.OneToOneField(User)
    nome = models.CharField(max_length=255)

class Entrada(models.Model):
    """
    Item da caixa de entrada
    """
    titulo = models.CharField(max_length=255)
    nr = models.CharField(max_length=255)
    tipo = models.CharField(max_length=255)
    meta = models.TextField(default='')
    lido = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True)
    